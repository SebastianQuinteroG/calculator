import React from 'react';

class Calculadora extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValues: '',
      screen: '',
      value:'',
    }
    this.addNumber = this.addNumber.bind(this);
  }

  addNumber = (numero) => {
    this.screen += numero.target.value;
    this.inputValues += numero.target.value;
  };

  reset = () => {
    this.screen = "";
    this.inputValues = "";
    this.setState({value:""});
  };

  handleResult = () => {
    fetch("https://localhost:44371/calculadora/"+this.inputValues)
      .then(res => res.json())
      .then(data => {
        this.setState({value: data })
        this.inputValues = data;
      })
  };

  render(){
    return (
      <>
        <h1 className="title">Calculator</h1>
        <div className="calculator">
          <label className="screen" text={this.state.value} id="screen"> {this.state.value}</label>
          <ul id="buttons" className="buttons">
            <button onClick={this.reset} className="btn" value="clear">C</button>
            <button className="btn"/>
            <button className="btn"/>
            <button className="btn"/>
            <button onClick={this.addNumber} className="btn" value="7">7</button>
            <button onClick={this.addNumber} className="btn" value="8">8</button>
            <button onClick={this.addNumber} className="btn" value="9">9</button>
            <button onClick={this.addNumber} className="btn" value="_">-</button>
            <button onClick={this.addNumber} className="btn" value="4">4</button>
            <button onClick={this.addNumber} className="btn" value="5">5</button>
            <button onClick={this.addNumber} className="btn" value="6">6</button>
            <button onClick={this.addNumber} className="btn" value="|">+</button>
            <button onClick={this.addNumber} className="btn" value="1">1</button>
            <button onClick={this.addNumber} className="btn" value="2">2</button>
            <button onClick={this.addNumber} className="btn" value="3">3</button>
            <button onClick={this.handleResult} className="btn equal tall">=</button>
            <button onClick={this.addNumber} className="btn wide shift" value="0">0</button>
            <button onClick={this.addNumber} className="shift" value=",">,</button>
          </ul>
        </div>
      </>
    );
  }
};

export default Calculadora;
