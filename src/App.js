import React from 'react';
import './App.css';
import Calculadora from './components/Calculator/Calculator';

function App() {
  return (
    <Calculadora />
  );
}

export default App;
